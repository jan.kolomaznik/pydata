Datová nalýza pomocí knihovny Pandas
=====================================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Chcete se dozvědět více informací z vašich dat? Už vám nestačí zpracovávat data pomocí Excelu a SQL a raději byste získali nad zpracováním dat plnou kontrolu? 
Rádi byste celý postup zpracování dat automatizovali, abyste se mohl vrhnout na další zajímavá data? 
Přihlaste se na praktický workshop, ve kterém si vyzkoušíte zpracování dat pomocí knihovny Pandas a kreslení grafů knihovnou Matplotlib. 
Napište si celý postup zpracování dat jako dokument v praktickém nástroji Jupyter Notebook prokládaný jednotlivými kroky, ve kterých data zpracováváte, vykreslujete a znázorňujete v grafech..

Náplň kurzu:
------------

- **[Úvod do datové analýzy (data sience)](00-pandas.ipynb)**
    - Proces analýzy dat
    - Pandas
    - Příprava dat
    - Ověření hypotézy
- **[Jupyter notebook](01-jupyter.ipynb)**
    - Instalace a spuštení
    - Ovládaní
- **[Základní datové typy](02-pandas.types.ipynb)**
    - Posloupnost (Series)
    - Konverze serie
    - Tabulky (DataFrame)
    - Zjišťování základních informací
- **[Základní funkce pro práci s daty](03-pandas.math.ipynb)**
    - Matematické operace
    - Funkce Jednoduché statistické funkce
    - DataFrame
- **[Základní logické operace s Daty](04-pandas.mask.ipynb)**
    - Transformace na masku
    - Masky
- **[Řezy a výběry](05-pandas.slice.ipynb)**
    - Výběry částí
    - Intexy, řezy a masky
    - Metoda `loc` a `iloc`
    - Dataframe
    - Indexer `loc`
    - Indexer `iloc`
- **[Práce s řetězci](06-pandas.str.ipynb)**
    - Manipulace s řetezci
    - Dotazování se na řetězce

- **[Sloupce (Series)](07-pandas.series.ipynb)**
    - Spojování serii
    - Další metody
    - Další metody serie
- **[Tabulky (DataFrame)](08-pandas.dataframe.ipynb)**
    - Osy DataFrame
    - Mazání z DaraFrame
    - Přejmenování
    - Indexy
    - Merge
    - Groupby
    - Pivot table
  * Statistické vlastnosti dat
  * Ověřování korelací
- **[Grafy](09-pandas.plot.ipynb)**
    - Přehled základních grafů:
- **Kam pokračovat dál**
  * Další knihovny a nástroje
  * Možnosti strojového učení
  
