# Lineární regrese
V následujícím příkladě použijeme lineární regresi pomocí knihovny **scikit-learn**.

Budeme se snažit namodelovat vytápění budoby v závislosti na venkovní teplote.

## Nastavení prostředí
Pro výpočet použijeme Jupyther notebook.

Nejprve naimportujeme všechny potřebné knihovny a nastavíme prostředí.

```python
import pandas as pd
import numpy as np
from scipy import stats
from datetime import datetime
from sklearn import preprocessing
from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
%matplotlib inline
```

## Vstupní Data
Vstupní data jsou uložená v souboru `building1retail.csv`.

Jejich nactení provedem pomocí příkazu `pd.read_csv()`

Protože data obsahují datum ve formátu `1/1/2010 1:15`, musíme říci knihovně *Pandas* jak je zpracovat.

Také bude chtít převést první sloupec s daty jako **index**.

```python
filename = 'building1retail.csv'
df = pd.read_csv(filename, index_col=[0], 
                 date_parser=lambda x: datetime.strptime(x, "%m/%d/%Y %H:%M"))
df.head()
```

## Zpracová dat
První krokem by vždy měla být analýza dat.

Je vždý užitečné se podívat jak dta vypadají. 

Pandas umožňuje velmi lehce vytořit graf:

```python
df.plot(figsize=(18,5))
```

Z grafu zjistíme:
- Rozsah a dimezi dat
- Můžeme si všimnout chybějícíh dat
- Anomálií v v pruběhu.

### Chybějící data
V podstatě v každém vzorku dat, který kdy bydete zpracovávat najdete chybějící data.

Tyto data je možné:
- **Odtranit**: Odstraění je jednodužší způsob.
  Pokud používáte analýzy založené na ML je toto korektní možnost. 
- **Dopočítat**: V případě analyzý založené na statisckých výpočtech je **odstranění dat nepřípustné**.
  Je potřeba dopočítat tadata taky aby jste neporušili statiský vzotek.
  
V následujícm případě pouze otestoruji, jestli vzorek obahuje chybějící hodnoty.

```python
df.isnull().values.any()
```

### Zpracování anomálií
Anomálie v datech se ve statistickém světě nazývají „outliers“. 

Krajní odlehlé hodnoty jsou většinou (ne vždy) výsledkem experimentální chyby (chybná funkce elektroměru by mohla být pravděpodobnou příčinou) nebo by mohla být správná hodnota? 

V každém případě je lepší ji zlikvidovat.

Pokud data odpovídají normální distribuci, můžemestats použít pravidlo 68–95–99.7 k odstranění odlehlých hodnot.

Nejdříve si však nejprve ověřte náš předpoklad (nezapomeňte - vždy analyzyjte data a znivh byvozujte své závěry, nikdy neprovádějte žádné předpoklady)
 
Následuje ověření, že data jsou skutečne v notmálním rozloženi:

```python
df.hist()
```

Metoda `.hist()` vytvoří jeden histogram, čímž se získá grafické znázornění distribuce dat.
 
Grafy ukazují, že data zhruba odpovídají normálnímu rozdělení.

Nyní odtraníme všechny hodnoty, které jsou větší než 3 standardní odchylky od střední hodnoty a vykreslíme nový dataframe.

```python
std_dev = 3
df = df[(np.abs(stats.zscore(df)) < std_dev).all(axis=1)]
df.plot(figsize=(18,5))
```


```python
df["OAT"] = (df["OAT (F)"] - 32) / 1.8
df.drop(columns=["OAT (F)"], inplace=True)
df.plot(subplots=True,figsize=(18,8))
```

### Ověření lineárního vztahu
Pokud chceme použít lineátní regresi, měly by existovat lineární vztah mezi daty.

Rovněž platí že je lepší si to potvrdit.

K romu je možné pužít **scatter graf**.

```python
plt.scatter(df['OAT'], df['Power (kW)'])
```

### Zobrazení jednoho dne
Také se můžeme podívat na průběh jednho dne. 

Takto mužeme oběřit, jestli data a časy v datasetu nejsou "posunuty"

Pojďme si vybrat náhodný den, řekněme - 4. března 2010 (čtvrtek).

```python
df.loc['2010-03-04', ['OAT']].plot()

# Prints the day
print(df.loc['2010-03-04'].index.day_name()[0])
```

OAT začíná stoupat po východu slunce (~ 6: 30 ráno) a klesá po západu slunce (17:30) - což dává celkový smysl. 

Proto můžeme usuzovat, že data jsou v pořádku.

Pro pro názornost si můžeme pro stejný den zobrazit i tepelný výkon kotle budovy.
```python
df.loc['2010-03-04', ['Power (kW)']].plot()
print(df.loc['2010-03-04'].index.day_name()[0])
```

## Lineární regrese
Dalším krokem je natrénovaní modelu pomocí Scikit-Learn.

Pro trénovaní budeme potřebovat vytvořit z dat dvě serie, jednu vstup do modelu (X) a jednu pro výstup (Y)

```python
x = pd.DataFrame(df['OAT'])
y = pd.DataFrame(df['Power (kW)'])
```

Pro určení kvality moduelu použijeme **k-fold cross validation**.

Tento přístup je obyčejně používán v aplikovaném strojovém učení a umožňuje porovnat a vybrat model pro daný prediktivní modelování problém.
 
Je to metoda zjišťování, jak moc bude model statistické analýzy ovlivňovat nezávislé vzorky dat. 

Tento postup je významný pro predikci neznámých vzorků po předchozí klasifikaci známých vzorků. 

### Princip křížové validace
Vstupní množina dat je rozdělena na podmnožiny. 
Jedna podmnožina slouží jako testovací množina, zbylé podmnožiny slouží jako trénovací množiny. 
Klasifikátor natrénuje model na trénovací množině a pomocí testovací množiny testuje přesnost a výkonnost tohoto modelu. 
Tento proces se několikrát opakuje, pokaždé s jinou podmnožinou tvořící trénovací a testovací množinu. 

### Vytvoření modelu a trénovacích sad
Vytvoříme model: `LinearRegression()`.
A data rozdělíme do tří sad.

```python
model = LinearRegression()
scores = []
kfold = KFold(n_splits=3, shuffle=True)
```

Provedeme trénovaní našeho modelu
```python
for i, (train, test) in enumerate(kfold.split(x, y)):
    model.fit(x.iloc[train,:], y.iloc[train,:])
    score = model.score(x.iloc[test,:], y.iloc[test,:])
    scores.append(score)

print(scores)
```
   
Výsledná score nevypdají dobře, to znamená že model se dobře nanučil predikovat.

## Lineární regrese 2
Jak můžeme model vylepšit? 
V analýzy dat jsme si mohli všimnout, že výkon kotle není jen závislý na venkovní teplotě ale i na hodinách.

Tuto informaci musíme začlenit také do našeho regresního modelu.

Vstupní data rosšíříme o informaci, o jakou hodnu se jedná.
Jedná se defakto o výčtová data (hodiny na sobě nejou nijak závislé), prtoto je lepší je rozložit do samostaných sloupců-


```python
x = pd.DataFrame(df['OAT'])
x["tod"] = x.index.hour

# drop_first = True removes multi-collinearity
add_var = pd.get_dummies(x["tod"], prefix="tod", drop_first=True)

# Add all the columns to the model data
x = x.join(add_var)

# Drop the original column that was expanded
x.drop(columns=["tod"], inplace=True)

x.head()
```

Výsledky by nyní měli být výrazně lepší.

## Použití modelu
Když máme model natrénován můžeme jej použit pro predikci.

Zeněme si jeden den ze vzorku dat a použijme pro něj náš model

```python
day = '2010-03-04'
test_day = y.loc[day, :]
test_day["model"] = model.predict(x.loc[day, :])
test_day.plot()
```

