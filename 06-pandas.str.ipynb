{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "6fc91ba7",
   "metadata": {},
   "source": [
    "# Práce s řetězci"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "e8c9e258",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f354b837",
   "metadata": {},
   "outputs": [],
   "source": [
    "got = pd.read_csv(\"static/game_of_throne.csv\")\n",
    "got"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2add7ca5",
   "metadata": {},
   "source": [
    "Řetězcové operace se u řetězcových sloupců schovávají pod \"jmenným prostorem\" `str`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "d390a867",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0     ------------------------67--------------------...\n",
       "1     ------------------------62--------------------...\n",
       "2     ------------------------62--------------------...\n",
       "3     ------------------------61--------------------...\n",
       "4     ------------------------59--------------------...\n",
       "5     ------------------------59--------------------...\n",
       "6     ------------------------56--------------------...\n",
       "7     ------------------------52--------------------...\n",
       "8     ------------------------48--------------------...\n",
       "9     ------------------------47--------------------...\n",
       "10    ------------------------45--------------------...\n",
       "11    ------------------------42--------------------...\n",
       "12    ------------------------41--------------------...\n",
       "13    ------------------------40--------------------...\n",
       "14    ------------------------39--------------------...\n",
       "15    ------------------------38--------------------...\n",
       "16    ------------------------34--------------------...\n",
       "17    ------------------------29--------------------...\n",
       "18    ------------------------27--------------------...\n",
       "19    ------------------------26--------------------...\n",
       "20    ------------------------26--------------------...\n",
       "21    ------------------------25--------------------...\n",
       "22    ------------------------24--------------------...\n",
       "23    ------------------------21--------------------...\n",
       "24    ------------------------20--------------------...\n",
       "25    ------------------------16--------------------...\n",
       "26    ------------------------12--------------------...\n",
       "27    ------------------------9---------------------...\n",
       "28    ------------------------7---------------------...\n",
       "Name: episodes, dtype: object"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "got['episodes'].astype(str).str.center(50, '-')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "7bffdef3",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0     :::::::::::::::::Tyrion Lannister:::::::::::::...\n",
       "1     ::::::::::::::::Daenerys Targaryen::::::::::::...\n",
       "2     :::::::::::::::::Cersei Lannister:::::::::::::...\n",
       "3     :::::::::::::::::::::Jon Sníh:::::::::::::::::...\n",
       "4     ::::::::::::::::::::Arya Stark::::::::::::::::...\n",
       "5     :::::::::::::::::::Sansa Stark::::::::::::::::...\n",
       "6     :::::::::::::::::Jaime Lannister::::::::::::::...\n",
       "7     ::::::::::::::::::Jorah Mormont:::::::::::::::...\n",
       "8     ::::::::::::::::::Samwell Tarly:::::::::::::::...\n",
       "9     ::::::::::::::::::Theon Greyjoy:::::::::::::::...\n",
       "10    ::::::::::::::::::::Lord Varys::::::::::::::::...\n",
       "11    :::::::::::::::::::Davos Mořský:::::::::::::::...\n",
       "12    :::::::::::::::::Brienne z Tarthu:::::::::::::...\n",
       "13    ::::::::::::::Petyr 'Malíček' Baeliš::::::::::...\n",
       "14    ::::::::::::::::::Brandon Stark:::::::::::::::...\n",
       "15    ::::::::::::::Sandor 'Ohař' Clegane:::::::::::...\n",
       "16    ::::::::::::::::Tormund Obrozhouba::::::::::::...\n",
       "17    ::::::::::::::::::::Melisandra::::::::::::::::...\n",
       "18    :::::::::::::::::Tywin Lannister::::::::::::::...\n",
       "19    ::::::::::::::::Joffrey Baratheon:::::::::::::...\n",
       "20    :::::::::::::::::Margaery Tyrell::::::::::::::...\n",
       "21    ::::::::::::::::::Catelyn Stark:::::::::::::::...\n",
       "22    ::::::::::::::::Stannis Baratheon:::::::::::::...\n",
       "23    ::::::::::::::::::::Robb Stark::::::::::::::::...\n",
       "24    ::::::::::::::::::Ramsay Bolton:::::::::::::::...\n",
       "25    :::::::::::::::::Tommen Baratheon:::::::::::::...\n",
       "26    :::::::::::::::::Nejvyšší vrabčák:::::::::::::...\n",
       "27    ::::::::::::::::Eddard 'Ned' Stark::::::::::::...\n",
       "28    :::::::::::::::::Robert Baratheon:::::::::::::...\n",
       "Name: role, dtype: object"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "got['role'].str.center(50, \":\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6a8b629e",
   "metadata": {},
   "source": [
    "Další metody pro transformacivi stringu jsou:\n",
    "* `str.lower()`\n",
    "* `str.upper()`\n",
    "* `str.title()`\n",
    "* `str.capitalize()`\n",
    "* `str.casefold()`\n",
    "* `str.swapcase()`\n",
    "\n",
    "\n",
    "## Manipulace s řetezci\n",
    "Pokud potřebujete řetězce ve seriích spojit, můžete použít operaci `+` nebo `str.join()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "af4cf9ed",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0        [lion, elephant, zebra]\n",
       "1                [1.1, 2.2, 3.3]\n",
       "2                [cat, nan, dog]\n",
       "3               [cow, 4.5, goat]\n",
       "4    [duck, [swan, fish], guppy]\n",
       "dtype: object"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s = pd.Series([['lion', 'elephant', 'zebra'],\n",
    "               [1.1, 2.2, 3.3],\n",
    "               ['cat', np.nan, 'dog'],\n",
    "               ['cow', 4.5, 'goat'],\n",
    "               ['duck', ['swan', 'fish'], 'guppy']])\n",
    "s"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "93401a2d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'lion ---> elephant ---> zebra'"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "' ---> '.join(['lion', 'elephant', 'zebra'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "fdde8179",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0    lion ---> elephant ---> zebra\n",
       "1                              NaN\n",
       "2                              NaN\n",
       "3                              NaN\n",
       "4                              NaN\n",
       "dtype: object"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s.str.join(' ---> ')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c05854ba",
   "metadata": {},
   "source": [
    "Opakem `str.join()` je `str.split`.\n",
    "U něj můžem pomocí parametru `expand=True` nastavit rozdelení řetězce do více `Serie`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "fcf2a2f1",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0                       this is a regular sentence\n",
       "1    https://docs.python.org/3/tutorial/index.html\n",
       "2                                              NaN\n",
       "dtype: object"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s = pd.Series([\n",
    "        \"this is a regular sentence\",\n",
    "        \"https://docs.python.org/3/tutorial/index.html\",\n",
    "        np.nan\n",
    "    ])\n",
    "s"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "4dc9a059",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>0</th>\n",
       "      <th>1</th>\n",
       "      <th>2</th>\n",
       "      <th>3</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>this is a regular sentence</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>https:</td>\n",
       "      <td></td>\n",
       "      <td>docs.python.org</td>\n",
       "      <td>3/tutorial/index.html</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                            0     1                2                      3\n",
       "0  this is a regular sentence  None             None                   None\n",
       "1                      https:        docs.python.org  3/tutorial/index.html\n",
       "2                         NaN   NaN              NaN                    NaN"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s.str.split('/', n=3, expand=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91214e16",
   "metadata": {},
   "source": [
    "U split můžeme nstavit:\n",
    "* `n` jako maximální počet částín na který se má řetězec rozložit\n",
    "* `pat` je řetězec nebo regulární výraz podle kterého se bude dělit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a14b84a",
   "metadata": {},
   "outputs": [],
   "source": [
    "s.str.split(n=3, pat=r\"[/ ]\", expand=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "9c3bf304",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>0</th>\n",
       "      <th>1</th>\n",
       "      <th>2</th>\n",
       "      <th>3</th>\n",
       "      <th>4</th>\n",
       "      <th>5</th>\n",
       "      <th>6</th>\n",
       "      <th>7</th>\n",
       "      <th>8</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>Rok založení</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>1892</th>\n",
       "      <td>SK Slavia Praha[p 1]</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1893</th>\n",
       "      <td>FK Loučeň 1893</td>\n",
       "      <td>AC Sparta Praha[p 2]</td>\n",
       "      <td>ČSK Uherský Brod</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1894</th>\n",
       "      <td>SK Spartak Příbram</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1896</th>\n",
       "      <td>FK Meteor Praha</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1898</th>\n",
       "      <td>SK Český Brod</td>\n",
       "      <td>SK Jičín</td>\n",
       "      <td>SK Praha VII (FK Loko Vltavín)</td>\n",
       "      <td>SK Klatovy 1898</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1899</th>\n",
       "      <td>ČAFC Královské Vinohrady</td>\n",
       "      <td>AFK Chrudim</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1900</th>\n",
       "      <td>SK Polaban Nymburk</td>\n",
       "      <td>SK České Budějovice</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1901</th>\n",
       "      <td>SK Brandýs nad Labem</td>\n",
       "      <td>SK Náchod</td>\n",
       "      <td>SK Kralupy nad Vltavou</td>\n",
       "      <td>SK Olympia Hradec Králové</td>\n",
       "      <td>SK Říčany</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1902</th>\n",
       "      <td>FK Mladá Boleslav</td>\n",
       "      <td>SK Stella Čáslav</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1903</th>\n",
       "      <td>SK Viktoria Žižkov</td>\n",
       "      <td>SK Kladno</td>\n",
       "      <td>SK Rakovník</td>\n",
       "      <td>SK Nusle</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1904</th>\n",
       "      <td>1. SK Prostějov</td>\n",
       "      <td>SK Semily</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1905</th>\n",
       "      <td>AFK Vršovice (Bohemians)</td>\n",
       "      <td>SK Hradec Králové</td>\n",
       "      <td>SK Čechie Smíchov,FK Mělník</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1906</th>\n",
       "      <td>SK Moravská Slavia Brno</td>\n",
       "      <td>SK Sparta Kladno</td>\n",
       "      <td>FC Velké Meziříčí</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1907</th>\n",
       "      <td>AFK Union Žižkov</td>\n",
       "      <td>Novoměstský SK Kladno</td>\n",
       "      <td>FK Slavoj Vyšehrad</td>\n",
       "      <td>SK Roudnice nad Labem</td>\n",
       "      <td>TJ Čechie Zastávka</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1908</th>\n",
       "      <td>SK Jaroměř</td>\n",
       "      <td>SK Sparta Košíře</td>\n",
       "      <td>SK Husovice</td>\n",
       "      <td>AFK Pečky</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1909</th>\n",
       "      <td>SK Kročehlavy</td>\n",
       "      <td>SK Most</td>\n",
       "      <td>AFK Přelouč</td>\n",
       "      <td>FK Admira Praha</td>\n",
       "      <td>FC Slovan Rosice</td>\n",
       "      <td>Spartak Hrdlořezy</td>\n",
       "      <td>SK Praga Vysočany</td>\n",
       "      <td>Nuselský SK</td>\n",
       "      <td>SK Mělník</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1910</th>\n",
       "      <td>FC Písek</td>\n",
       "      <td>FK Jindřichův Hradec 1910</td>\n",
       "      <td>SK Smíchov Plzeň</td>\n",
       "      <td>TJ Jiskra Litomyšl</td>\n",
       "      <td>DSK Třebíč</td>\n",
       "      <td>TJ Baník Švermov</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1911</th>\n",
       "      <td>SK Viktoria Plzeň</td>\n",
       "      <td>FK Dobrovice</td>\n",
       "      <td>SK Moravské Budějovice</td>\n",
       "      <td>SK Střešovice 1911</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1912</th>\n",
       "      <td>FC Ivančice</td>\n",
       "      <td>SK Sparta Kolín</td>\n",
       "      <td>SK Butovice (Motorlet)</td>\n",
       "      <td>AFK Tišnov</td>\n",
       "      <td>TJ FS Napajedla</td>\n",
       "      <td>FK Agria Choceň</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1913</th>\n",
       "      <td>SK Židenice (Zbrojovka Brno)</td>\n",
       "      <td>SK Benešov</td>\n",
       "      <td>FK Luhačovice</td>\n",
       "      <td>SK Slovan Dubí</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1914</th>\n",
       "      <td>FK Králův Dvůr</td>\n",
       "      <td>TJ Jiskra Ústí nad Orlicí</td>\n",
       "      <td>ABC Braník</td>\n",
       "      <td>FK Zlíchov 1914</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "      <td>None</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                                         0                          1  \\\n",
       "Rok založení                                                            \n",
       "1892                  SK Slavia Praha[p 1]                       None   \n",
       "1893                        FK Loučeň 1893       AC Sparta Praha[p 2]   \n",
       "1894                    SK Spartak Příbram                       None   \n",
       "1896                       FK Meteor Praha                       None   \n",
       "1898                         SK Český Brod                   SK Jičín   \n",
       "1899              ČAFC Královské Vinohrady                AFK Chrudim   \n",
       "1900                    SK Polaban Nymburk        SK České Budějovice   \n",
       "1901                  SK Brandýs nad Labem                  SK Náchod   \n",
       "1902                     FK Mladá Boleslav           SK Stella Čáslav   \n",
       "1903                    SK Viktoria Žižkov                  SK Kladno   \n",
       "1904                       1. SK Prostějov                  SK Semily   \n",
       "1905              AFK Vršovice (Bohemians)          SK Hradec Králové   \n",
       "1906               SK Moravská Slavia Brno           SK Sparta Kladno   \n",
       "1907                      AFK Union Žižkov      Novoměstský SK Kladno   \n",
       "1908                            SK Jaroměř           SK Sparta Košíře   \n",
       "1909                         SK Kročehlavy                    SK Most   \n",
       "1910                              FC Písek  FK Jindřichův Hradec 1910   \n",
       "1911                     SK Viktoria Plzeň               FK Dobrovice   \n",
       "1912                           FC Ivančice            SK Sparta Kolín   \n",
       "1913          SK Židenice (Zbrojovka Brno)                 SK Benešov   \n",
       "1914                        FK Králův Dvůr  TJ Jiskra Ústí nad Orlicí   \n",
       "\n",
       "                                           2                          3  \\\n",
       "Rok založení                                                              \n",
       "1892                                    None                       None   \n",
       "1893                        ČSK Uherský Brod                       None   \n",
       "1894                                    None                       None   \n",
       "1896                                    None                       None   \n",
       "1898          SK Praha VII (FK Loko Vltavín)            SK Klatovy 1898   \n",
       "1899                                    None                       None   \n",
       "1900                                    None                       None   \n",
       "1901                  SK Kralupy nad Vltavou  SK Olympia Hradec Králové   \n",
       "1902                                    None                       None   \n",
       "1903                             SK Rakovník                   SK Nusle   \n",
       "1904                                    None                       None   \n",
       "1905             SK Čechie Smíchov,FK Mělník                       None   \n",
       "1906                       FC Velké Meziříčí                       None   \n",
       "1907                      FK Slavoj Vyšehrad      SK Roudnice nad Labem   \n",
       "1908                             SK Husovice                  AFK Pečky   \n",
       "1909                             AFK Přelouč            FK Admira Praha   \n",
       "1910                        SK Smíchov Plzeň         TJ Jiskra Litomyšl   \n",
       "1911                 SK Moravské Budějovice          SK Střešovice 1911   \n",
       "1912                  SK Butovice (Motorlet)                 AFK Tišnov   \n",
       "1913                           FK Luhačovice             SK Slovan Dubí   \n",
       "1914                              ABC Braník            FK Zlíchov 1914   \n",
       "\n",
       "                               4                  5                  6  \\\n",
       "Rok založení                                                             \n",
       "1892                        None               None               None   \n",
       "1893                        None               None               None   \n",
       "1894                        None               None               None   \n",
       "1896                        None               None               None   \n",
       "1898                        None               None               None   \n",
       "1899                        None               None               None   \n",
       "1900                        None               None               None   \n",
       "1901                   SK Říčany               None               None   \n",
       "1902                        None               None               None   \n",
       "1903                        None               None               None   \n",
       "1904                        None               None               None   \n",
       "1905                        None               None               None   \n",
       "1906                        None               None               None   \n",
       "1907          TJ Čechie Zastávka               None               None   \n",
       "1908                        None               None               None   \n",
       "1909            FC Slovan Rosice  Spartak Hrdlořezy  SK Praga Vysočany   \n",
       "1910                  DSK Třebíč   TJ Baník Švermov               None   \n",
       "1911                        None               None               None   \n",
       "1912             TJ FS Napajedla    FK Agria Choceň               None   \n",
       "1913                        None               None               None   \n",
       "1914                        None               None               None   \n",
       "\n",
       "                        7          8  \n",
       "Rok založení                          \n",
       "1892                 None       None  \n",
       "1893                 None       None  \n",
       "1894                 None       None  \n",
       "1896                 None       None  \n",
       "1898                 None       None  \n",
       "1899                 None       None  \n",
       "1900                 None       None  \n",
       "1901                 None       None  \n",
       "1902                 None       None  \n",
       "1903                 None       None  \n",
       "1904                 None       None  \n",
       "1905                 None       None  \n",
       "1906                 None       None  \n",
       "1907                 None       None  \n",
       "1908                 None       None  \n",
       "1909          Nuselský SK  SK Mělník  \n",
       "1910                 None       None  \n",
       "1911                 None       None  \n",
       "1912                 None       None  \n",
       "1913                 None       None  \n",
       "1914                 None       None  "
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "kluby = pd.read_html(\"https://cs.wikipedia.org/wiki/Seznam_nejstar%C5%A1%C3%ADch_fotbalov%C3%BDch_klub%C5%AF_v_%C4%8Cesku\")[0]\n",
    "kluby = kluby.set_index(\"Rok založení\")['Název']\n",
    "kluby.str.split(', ', expand=True)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e68771d7",
   "metadata": {},
   "source": [
    "> #### Příklad\n",
    "> Se serie *got* rozložte jéma herců do dvou sloubců: 'name' a 'surname'."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "18980dd8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# řešení\n",
    "split = got['actor'].str.split(n=1, expand=True)\n",
    "got['name'] = split[0]\n",
    "got['surname'] = split[1]\n",
    "got"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "267f795d",
   "metadata": {},
   "outputs": [],
   "source": [
    "split = got['actor'].str.split(pat=r\" (.+ )?\", expand=True)\n",
    "got['name'] = split[0]\n",
    "got['surname'] = split[2]\n",
    "del split\n",
    "got"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7ddbd540",
   "metadata": {},
   "source": [
    "Další metoda z této skupiny je `str.cat()`, která spojí řetězce v serii s položkami v jiné serii podle idenxů."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d64d5190",
   "metadata": {},
   "outputs": [],
   "source": [
    "s = pd.Series(['a', 'b', np.nan, 'd'])\n",
    "s.str.cat(sep=' ')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f182dea7",
   "metadata": {},
   "outputs": [],
   "source": [
    "s.str.cat(['A', 'B', 'C', 'D'], sep='-', na_rep='?')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d8a664ef",
   "metadata": {},
   "outputs": [],
   "source": [
    "import re\n",
    "\n",
    "def foo(match:re.Match):\n",
    "    return match.group(1).lower()\n",
    "    \n",
    "died = got['died'].dropna()\n",
    "died.str.replace(r\"(S\\d+), (E\\d+)\", foo)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f06e986d",
   "metadata": {},
   "source": [
    "Pro upavu řetezců se hodí funkce `str.replace()`, která umožňuje nahradit část řetězce jinou částí.\n",
    "Mezi základní rysy této funkce patří:\n",
    "\n",
    "1. Vyhledávaná část může být *regex*\n",
    "2. Jako náhrada se dá použít: řetězec, který může obsahovat skupiny z regex nebo dokonce *funkce*\n",
    "\n",
    "> #### Příklad\n",
    "> z rolí v *got* odstraňtě všechny přezdívky"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "38decc97",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Řešení\n",
    "got['role'].str.replace(r\"'.+'\\s\", \"\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8f4fdd21",
   "metadata": {},
   "outputs": [],
   "source": [
    "got['role'].str.find('Stark')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e36ceef4",
   "metadata": {},
   "source": [
    "## Dotazování se na řetězce\n",
    "Pokud je potřeba z řetězce získat nějakou informaci, je to možné pomocí metod `str.find()`, `str.match()`, `str.contains()` a dalších.\n",
    "\n",
    "Přičem metoda `str.match` pracuje s regulárními výrazy.\n",
    "\n",
    "> #### Příklad\n",
    "> z *got* vyberte ty role, které mají přezdívku."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "63baffc2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# řešení"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3b8767b",
   "metadata": {},
   "source": [
    "Další užitečnou funkcí z této skupiny je `str.extract()`, která rovněž vyučívá regulírních výrazů aby v řrtězc našla informace a vytáhla je do jedné nebo více serii."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b66f1f44",
   "metadata": {},
   "outputs": [],
   "source": [
    "s = pd.Series(['a1', 'b2', 'c3'])\n",
    "s.str.extract(r'([ab])(\\d)')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3bcd66ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "s.str.extract(r'(?P<letter>[ab])(?P<digit>\\d)')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a11eba8b",
   "metadata": {},
   "source": [
    "> #### Příklad\n",
    "> Z tabulky *got* u výtáhněte informaci pro každou postavu\n",
    "> 1. jmeno do sloupice 'name'\n",
    "> 2. Rod, pokud se dá z jéma zjistit, do sloupice `house`\n",
    "> 3. Prezdivka 'nickname'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "faa90e4b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# řešení\n",
    "got['role'].str.extract(r\"(?P<name>.+?) ('(?P<nickname>.+)' )?(?P<house>.+)\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a5bd68f4",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": ".venv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
